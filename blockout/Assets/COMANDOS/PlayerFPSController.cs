using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(CharacterMovement))]
[RequireComponent(typeof(MouseLook))]

public class PlayerFPSController : MonoBehaviour
{
    private CharacterMovement characterMovement;
    private MouseLook mouseLook;
    private GunAiming gunAiming;
    private FireGun fireGun;


    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("OptimizedTree").GetComponent<MeshRenderer>().enabled = false;

        characterMovement = GetComponent<CharacterMovement>();
        mouseLook = GetComponent<MouseLook>();
        gunAiming = GetComponentInChildren<GunAiming>();
        fireGun = GetComponentInChildren<FireGun>();
    }

    private void Update()
    {
        movement();
        rotation();
        aiming();
        shooting();
    }

    private void movement()
    {
        //Movement
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }

    private void rotation()
    {
        //Rotation
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");

        mouseLook.handleRotation(hRotationInput, vRotationInput);
    }

    private void aiming()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            gunAiming.OnButtonDown();
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            gunAiming.OnButtonUp();
        }
    }

    private void shooting() {

        if (Input.GetKeyDown(KeyCode.R))
        { fireGun.OnReloadButtonDown(); }
        else
        {
            switch (fireGun.gunData.firetype)
            {
                case FIRETYPE.REPEATER:
                case FIRETYPE.SEMIAUTOMATIC:
                    fireGun.shoot(Input.GetButtonDown("Fire1"));
                    break;

                case FIRETYPE.AUTOMATIC:
                    fireGun.shoot(Input.GetButton("Fire1"));
                    break;


            }
        
        }
    }
}